import axios from "axios"
import { useContext, useEffect, useState } from "react"
import { json, Link } from "react-router-dom"
import { Context } from "./Product/Context"


function Home(props) {
  const [getData, setData] = useState([])
  const value = useContext(Context)
  useEffect(() => {
    axios.get("http://localhost:8080/laravel/laravel/public/api/product")
      .then(res => {
        setData(res.data.data)
      })
      .catch(error => console.log(error))
  }, [])
  let obj = {}
  function Handleid(e) {
    let idproduct = e.target.id
    let xx = localStorage.getItem("Cardid")
    let y = 1;
    if (xx) {
      obj = JSON.parse(xx)
      Object.keys(obj).map(function (key, index) {
        if (idproduct == key) {
          obj[key] += 1;
          y = 2
        }
      })
    }
    if (y == 1) {
      obj[idproduct] = 1
    }
    let tongqty = 0
    Object.keys(obj).map(function (key, index) {
      tongqty += obj[key]
    })
    value.qtycart(tongqty)
    localStorage.setItem("Cardid", JSON.stringify(obj))
  }

  function renderData() {
    if (Object.keys(getData).length > 0) {
      return Object.keys(getData).map((value, key) => {
        let img = JSON.parse(getData[value].image)
        return (
          <div className="col-sm-4" key={key}>
            <div className="product-image-wrapper">
              <div className="single-products">
                <div className="productinfo text-center">
                  <img src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" + getData[value].id_user + "/" + img[0]} alt="" />
                  <h2>{getData[value].price}</h2>
                  <p>Easy Polo Black Edition</p>
                  <Link id={getData[value].id} onClick={Handleid} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                </div>
                <div id="id1" className="product-overlay">
                  <div className="overlay-content">
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link id={getData[value].id} onClick={Handleid} className="btn btn-default add-to-cart" ><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                </div>
              </div>
              <div className="choose">
                <ul className="nav nav-pills nav-justified">
                  <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                  <li><Link to={"/product/productdetail/" + getData[value].id}><i className="fa fa-plus-square" />Add to product detail</Link></li>
                </ul>
              </div>
            </div>
          </div>
        )
      })
    }

  }
  return (
    <div className="features_items">
      <h2 className="title text-center">Features Items</h2>
      {renderData()}
    </div>
  )
}

export default Home;
