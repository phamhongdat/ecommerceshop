import axios from "axios"
import { useState } from "react"
import { useParams } from "react-router-dom"
import FormErrors from "../member/FormErrors"

function Comment(props) {
    const getcmtcon = props.getcmtcon
    let params = useParams()
    const [getmessage, setmessage] = useState("")
    const [errors, seterrors] = useState({})
    function Handleinput(e) {
        setmessage(e.target.value)
    }
    function HandleForm() {
        let errorSubmit = ({})
        let flag = true
        let login = localStorage.getItem("Object")
        login = JSON.parse(login)
        if (!login) {
            errorSubmit.login = "vui long dang nhap"
            flag = false
        }
        else {
            if (getmessage == "") {
                errorSubmit.message = "vui long nhap comment"
                flag = false
            }
        }
        if (!flag) {
            seterrors(errorSubmit)
        }
        else {
            seterrors({})
            const userDAta = JSON.parse(localStorage["login"])
            let url = "http://localhost:8080/laravel/laravel/public/api/blog/comment/" + params.id
            let accessToken = userDAta.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData()
            formData.append("id_blog", params.id)
            formData.append("id_user", userDAta.Auth.id)
            formData.append("id_comment", getcmtcon ? getcmtcon : 0)
            formData.append("comment", getmessage)
            formData.append("image_user", userDAta.Auth.avatar)
            formData.append("name_user", userDAta.Auth.name)
            axios.post(url, formData, config)
                .then(res => {
                    props.getcmt(res.data)
                })
        }
    }
    return (
        <div className="replay-box">
            <div className="row">
                <div className="col-sm-12">
                    <h2>Leave a replay</h2>
                    <FormErrors errors={errors}> </FormErrors>
                    <div className="text-area">
                        <div className="blank-arrow">
                            <label>Your Name</label>
                        </div>
                        <span>*</span>
                        <textarea name="message" rows={11} defaultValue={""} onChange={Handleinput} />
                        <a onClick={HandleForm} className="btn btn-primary"  >post comment</a>
                    </div>
                </div>
            </div>
        </div>
        // {/*/Repaly Box*/}
    );
}

export default Comment;