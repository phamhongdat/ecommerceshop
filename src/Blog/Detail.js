import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import Comment from "./Comment"
import Listcomment from "./Listcomment"
import Rate from "./Rate"

function BlogDetail(props) {
  let params = useParams()
  const [getData, setData] = useState([])
  const [getcomment, setcomment] = useState([])
  const [getcmtcon, setcmtcon] = useState()
  useEffect(() => {
    axios.get("http://localhost:8080/laravel/laravel/public/api/blog/detail/" + params.id)
      .then(res => {
        setData(res.data.data)
        setcomment(res.data.data.comment)
      })
      .catch(error => console.log(error))
  }, [])
  function getcmt(x) {
    setcomment(getcomment.concat(x.data))
  }
  function cmtcon(a) {
    setcmtcon(a)
  }
  function renderData() {
    return (
      <div>
        <div className="blog-post-area" >
          <h2 className="title text-center">Latest From our Blog</h2>
          <div className="single-blog-post">
            <h3>{getData.title}</h3>
            <div className="post-meta">
              <ul>
                <li><i className="fa fa-user" /> Mac Doe</li>
                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
              </ul>
              <span>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-o"></i>
              </span>
            </div>
            <a href>
              <img src={"http://localhost:8080/laravel/laravel/public/upload/Blog/image/" + getData.image} alt="" />
            </a>
            <p>
              {getData.description}</p> <br />
            <div className="pager-area">
              <ul className="pager pull-right">
                <li><a href="#">Pre</a></li>
                <li><a href="#">Next</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
  return (
    <div className="col-sm-9">
      {renderData()}
      <Rate />
      <div className="socials-share">
        <a href><img src="images/blog/socials.png" alt="" /></a>
      </div>{/*/socials-share*/}
      <Listcomment getcomment={getcomment} cmtcon={cmtcon} />
      <Comment getcmt={getcmt} getcmtcon={getcmtcon} />
    </div>
  );
}

export default BlogDetail;