import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import StarRatings from "react-star-ratings";
import FormErrors from "../member/FormErrors";

function Rate(props) {
    const [rating, setRating] = useState(0)
    const [errors, seterrors] = useState({})
    let params = useParams()
    function changeRating(newRating, name) {
        setRating(newRating)
        let errorSubmit = ({})
        let login = localStorage.getItem("Object")
        login = JSON.parse(login)
        if (!login) {
            errorSubmit.login = "vui long dang nhap"
            seterrors(errorSubmit)
        }
        else {
            seterrors({})
            const userDAta = JSON.parse(localStorage["login"])
            let url = "http://localhost:8080/laravel/laravel/public/api/blog/rate/" + params.id
            const ratedata = new FormData()
            ratedata.append("user_id", userDAta.Auth.id)
            ratedata.append("blog_id", params.id)
            ratedata.append("rate", newRating)
            let accessToken = userDAta.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            axios.post(url, ratedata, config)
                .then(res => {
                    console.log(res)
                })

        }
    }
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/laravel/public/api/blog/rate/" + params.id)
            .then(res => {
                let a = res.data.data
                if (Object.keys(a).length > 0) {
                    let sum = 0
                    let lengthrate = 0
                    Object.keys(a).map((value, key) => {
                        lengthrate = Object.keys(a).length
                        let soRate = (a[value].rate)
                        sum += soRate
                    })
                    setRating(sum / lengthrate)
                }
            })
            .catch(error => console.log(error))
    }, [])
    return (
        <div className="rating-area">
            <FormErrors errors={errors} />
            <ul className="ratings">
                <StarRatings
                    rating={rating}
                    starRatedColor="blue"
                    changeRating={changeRating}
                    numberOfStars={6}
                    name='rating'
                />
            </ul>
            <ul className="tag">
                <li>TAG:</li>
                <li><a className="color" href>Pink <span>/</span></a></li>
                <li><a className="color" href>T-Shirt <span>/</span></a></li>
                <li><a className="color" href>Girls</a></li>
            </ul>
        </div>
    );
}

export default Rate;