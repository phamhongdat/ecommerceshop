import axios from "axios"
import React, { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"

function Listcomment(props) {
  const getcomment = props.getcomment
  function Handleinput(e) {
    props.cmtcon(e.target.id)
  }
  function renderData() {
    if (getcomment.length > 0) {
      return getcomment.map((value, key) => {
        if (value.id_comment == 0) {
          return (
            <React.Fragment key={key}>
              <li className="media" >
                <a className="pull-left" href="#">
                  <img className="media-object" src={"http://localhost:8080/laravel/laravel/public/upload/user/avatar/" + value.image_user} alt="" />
                </a>
                <div className="media-body">
                  <ul className="sinlge-post-meta">
                    <li><i className="fa fa-user" />{value.name_user}</li>
                    <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                    <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                  </ul>
                  <p> {value.comment}</p>
                  <a id={value.id} className="btn btn-primary" onClick={Handleinput}><i className="fa fa-reply" />Replay</a>
                </div>
              </li>
              {getcomment.map((value2, j) => {
                if (value.id == value2.id_comment) {
                  return (
                    <li key={j} index={j} className="media second-media">
                      <a className="pull-left" href="#">
                        <img className="media-object" src={"http://localhost:8080/laravel/laravel/public/upload/user/avatar/" + value2.image_user} alt="" />
                      </a>
                      <div className="media-body">
                        <ul className="sinlge-post-meta">
                          <li><i className="fa fa-user" />{value2.name_user}</li>
                          <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                          <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                        </ul>
                        <p>{value2.comment}</p>
                        <a id={value.id} onClick={Handleinput} className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                      </div>
                    </li>
                  )
                }
              })}
            </React.Fragment>
          )
        }
      })
    }
  }
  return (
    <div className="response-area">
      <h2>3 RESPONSES</h2>
      <ul className="media-list">
        {renderData()}
      </ul>
    </div>
  );
}

export default Listcomment;