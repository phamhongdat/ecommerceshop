import axios from "axios"
import { useEffect, useState } from "react"

function Cart() {
    const [getData, setData] = useState({})
    let objcha = {}
    useEffect(() => {
        let url = "http://localhost:8080/laravel/laravel/public/api/product/cart"
        let DAta = localStorage.getItem("Cardid")
        axios.post(url, DAta)
            .then(res => {
                setData(res.data.data)
            })
    }, [])
    function cong(e) {
        const idproduct = e.target.id
        // copy ra data moi 
        let newData = [...getData];
        Object.keys(newData).map((key, index) => {
            if (idproduct == newData[key].id) {
                newData[key].qty += 1
            }
        })
        setData(newData)
        let objcon = localStorage.getItem("Cardid")
        if (objcon) {
            objcha = JSON.parse(objcon)
            Object.keys(objcha).map(function (key, index) {
                if (idproduct == key) {
                    objcha[key] += 1;
                }
            })
        }
        localStorage.setItem("Cardid", JSON.stringify(objcha))
    }
    function tru(e) {
        const idproduct = e.target.id
        let newData = [...getData];
        Object.keys(newData).map((key, index) => {
            if (idproduct == newData[key].id) {
                newData[key].qty -= 1
            }
            if (newData[key].qty < 1) {
                delete newData[key]
            }
        })
        setData(newData)
        let objcon = localStorage.getItem("Cardid")
        if (objcon) {
            objcha = JSON.parse(objcon)
            Object.keys(objcha).map(function (key, index) {
                if (idproduct == key) {
                    objcha[key] -= 1;
                }
                if (objcha[key] < 1) {
                    delete objcha[key]
                }

            })
        }
        localStorage.setItem("Cardid", JSON.stringify(objcha))
    }
    function Delete(e) {
        const idproduct = e.target.id
        let newData = [...getData];
        Object.keys(newData).map(function (key, index) {
            if (idproduct == newData[key].id) {
                delete newData[key];
            }
        })
        let newArray = newData.filter(function (v) { return v !== '' });
        setData(newArray);
        let objcon = localStorage.getItem("Cardid")
        if (objcon) {
            objcha = JSON.parse(objcon)
            Object.keys(objcha).map(function (key, index) {
                if (idproduct == key) {
                    delete objcha[key];
                }
            })
        }
        localStorage.setItem("Cardid", JSON.stringify(objcha))
    }
    function rendertong() {
        let tong = 0
        if (Object.keys(getData).length > 0) {
            return getData.map((value, key) => {
                tong = tong + getData[key].price * getData[key].qty
                return (
                    <li>Total <span className="spantong">${tong}</span></li>
                )
            })
        }
    }
    function renderData() {
        if (Object.keys(getData).length > 0) {
            return Object.keys(getData).map((value, key) => {
                let total = getData[value].price * getData[value].qty
                let img = JSON.parse(getData[value].image)
                return (
                    <tbody>
                        <tr className="trtable" key={key}>
                            <td className="cart_product">
                                <a href=""><img src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" + getData[value].id_user + "/" + img[0]} alt="" /></a>
                            </td>
                            <td className="cart_description">
                                <h4><a href="">{getData[value].name}</a></h4>
                                <p className="id">Web ID: {getData[value].id}</p>
                            </td>
                            <td className="cart_price">
                                <p className="idprice">${getData[value].price}</p>
                            </td>
                            <td className="cart_quantity">
                                <div className="cart_quantity_button">
                                    <a className="cart_quantity_up" onClick={cong} id={getData[value].id} > + </a>
                                    <input className="cart_quantity_input" type="text" name="quantity" value={getData[value].qty} autocomplete="off" size="2" />
                                    <a className="cart_quantity_down" onClick={tru} id={getData[value].id} > - </a>
                                </div>
                            </td>
                            <td className="cart_total">
                                <p className="cart_total_price"  >${total}</p>
                            </td>
                            <td className="cart_delete">
                                <a className="cart_quantity_delete" onClick={Delete} id={getData[value].id}><i className="fa fa-times" onClick={Delete} id={getData[value].id}></i></a>
                            </td>
                        </tr>
                    </tbody>
                )
            })
        }
    }
    return (
        <div>
            <table className="cart">
                <thead>
                    <tr className="">
                        <th className="image">Image</th>
                        <td class="description"></td>
                        <th className="price">Price</th>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                    </tr>
                </thead>
                {renderData()}
            </table>
            <section id="do_action">
                <div className="container">
                    <div className="heading">
                        <h3>What would you like to do next?</h3>
                        <p>Choose if you have a discount code or reward points you want to use or would like to estimate your
                        delivery cost.</p>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="total_area">
                                <ul>
                                    <li>Cart Sub Total <span>$59</span></li>
                                    <li>Eco Tax <span>$2</span></li>
                                    <li>Shipping Cost <span>Free</span></li>
                                    {rendertong()}
                                </ul>
                                <a className="btn btn-default update" href>Update</a>
                                <a className="btn btn-default check_out" href>Check Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default Cart;