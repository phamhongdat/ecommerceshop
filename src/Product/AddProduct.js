import axios from "axios";
import { useEffect, useState } from "react";
import FormErrors from "../member/FormErrors";

function AddProduct() {
    const [getinput, setinput] = useState({
        name: "",
        category: "",
        price: "",
        brand: "",
        status: "",
        company: "",
        detail: "",
        sale: ""
    })
    const [getbrand, setbrand] = useState({})
    const [getcategory, setcategory] = useState({})
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/laravel/public/api/category-brand")
            .then(res => {
                setcategory(res.data.category)
                setbrand(res.data.brand)
            })
            .catch(error => console.log(error))
    }, [])
    function optioncategory() {
        if (getcategory.length > 0) {
            return getcategory.map((value, key) => {
                return (
                    <option key={key} value={value.id}>{value.category}</option>
                )
            })
        }
    }
    function optionbrand() {
        if (getbrand.length > 0) {
            return getbrand.map((value, key) => {
                return (
                    <option key={key} value={value.id}>{value.brand}</option>
                )
            })
        }
    }
    function inputsale() {
        if (getinput.status == "0") {
            return (
                <input type="text" placeholder="Sale" name="sale" onChange={Handleinput} />
            )
        }
    }
    const [errors, seterrors] = useState({})
    function Handleinput(e) {
        const nameInput = e.target.name
        const valueInput = e.target.value
        setinput(state => ({ ...state, [nameInput]: valueInput }))
    }
    const [getfile, setfile] = useState("")
    function HandleFile(e) {
        setfile(e.target.files)
    }
    const Duoifile = ["png", "jpg", "jpeg", "PNG", "JPG"]
    function HandleForm(e) {
        e.preventDefault()
        let errorSubmit = {}
        let flag = true
        if (getinput.name == "") {
            errorSubmit.name = "vui long nhap name"
            flag = false
        }
        if (getinput.price == "") {
            errorSubmit.price = "vui long nhap price"
            flag = false
        }
        if (getinput.company == "") {
            errorSubmit.company = "vui long nhap company"
            flag = false
        }
        if (getinput.detail == "") {
            errorSubmit.detail = "vui long nhap detail"
            flag = false
        }
        if (getinput.status == "") {
            errorSubmit.status = "vui long nhap status"
            flag = false
        } else if (getinput.status == "0" && getinput.sale == "") {
            errorSubmit.sale = " vui lòng nhap sale"
            flag = false;
        }
        if (getfile == "") {
            errorSubmit.files = "vui long them hinh anh "
            flag = false
        } else {
            const nameFile = getfile[0].name.split(".", 3);
            const nameFiles = nameFile[1]
            if (getfile[0].size > 1024 * 1024) {
                errorSubmit.files = "anh lon hon 1mb"
                flag = false;
            } else if (Duoifile.includes(nameFiles) == false) {
                errorSubmit.files = "anh khong dung dinh dang"
                flag = false;
            }
        }
        if (!flag) {
            seterrors(errorSubmit)
        } else {
            seterrors({})
            const userDAta = JSON.parse(localStorage["login"])
            let accessToken = userDAta.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            let url = "http://localhost:8080/laravel/laravel/public/api/user/add-product"
            const formData = new FormData()
            formData.append("name", getinput.name)
            formData.append("price", getinput.price)
            formData.append("category", getinput.category)
            formData.append("brand", getinput.brand)
            formData.append("company", getinput.company)
            formData.append("detail", getinput.detail)
            formData.append("status", getinput.status)
            formData.append("sale", getinput.sale ? getinput.sale : 0)
            Object.keys(getfile).map((item, i) => {
                formData.append("file[]", getfile[item])
            })
            axios.post(url, formData, config)
                .then(res => {
                    if (res.data.errors) {
                        seterrors(res.data.errors)
                    } else {
                        alert("add product thanh cong")
                        console.log(res)
                    }
                })
        }

    }
    return (
        <div className="col-sm-4 col-sm-offset-1">
            <div className="login-form">
                <h2>Create product</h2>
                <FormErrors errors={errors}> </FormErrors>
                <form action="#" onSubmit={HandleForm} encType="multipart/form-data">
                    <input type="text" placeholder="Name" name="name" onChange={Handleinput} />
                    <input type="text" placeholder="Price" name="price" onChange={Handleinput} />
                    <select name="category" onChange={Handleinput}>
                        <option value="">category</option>
                        {optioncategory()}
                    </select><br /><br />
                    <select name="brand" onChange={Handleinput}>
                        <option value="">brand</option>
                        {optionbrand()}
                    </select><br /><br />
                    <select name="status" onChange={Handleinput}>
                        <option value="">status</option>
                        <option value={1}>new</option>
                        <option value={0}>sale</option>
                    </select><br /><br />
                    {inputsale()}
                    <input type="text" placeholder="Company" name="company" onChange={Handleinput} />
                    <input type="file" placeholder="anh" name="files" onChange={HandleFile} multiple />
                    <textarea name="detail" rows={11} placeholder="Detail" defaultValue={""} onChange={Handleinput} />
                    <button type="submit" className="btn btn-default">Signup</button>
                </form>
            </div>
        </div>
    );
}

export default AddProduct;