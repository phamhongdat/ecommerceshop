import axios from "axios"
import { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"

function MyProduct(props) {
    const [getData, setData] = useState([])
    const [id, setid] = useState([])
    function Handleinput(e) {
        props.idedit(e.target.id)
    }
    useEffect(() => {
        const userDAta = JSON.parse(localStorage["login"])
        setid(userDAta.Auth.id)
        let accessToken = userDAta.success.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        axios.get("http://localhost:8080/laravel/laravel/public/api/user/my-product", config)
            .then(res => {
                setData(res.data.data)
            })
            .catch(error => console.log(error))
    }, [])
    function renderProduct() {
        if (Object.keys(getData).length > 0) {
            return Object.keys(getData).map((value, key) => {
                let img = JSON.parse(getData[value].image)
                return (
                    <tbody>
                        <tr>
                            <td>{getData[value].id}</td>
                            <td>{getData[value].name}</td>
                            <td>
                                <img src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" + id + "/" + img[0]} />
                            </td>
                            <td>{getData[value].price}</td>
                            <Link to={"/product/editproduct/" + getData[value].id} class="cart_quantity_delete"  ><i class="glyphicon glyphicon-pencil" id={getData[value].id} ></i></Link>
                            <a><i id={getData[value].id} class="fa fa-times" onClick={deleteproduct} /></a>
                        </tr>
                    </tbody>
                )
            })
        }
    }
    function deleteproduct(e) {
        let xoa = e.target.id
        const userDAta = JSON.parse(localStorage["login"])
        let accessToken = userDAta.success.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        if (xoa.length > 0) {
            axios.get("http://localhost:8080/laravel/laravel/public/api/user/delete-product/" + xoa, config)
                .then((res) => {
                    console.log(res)
                })
        }
    }
    return (
        <table className="myproduct">
            <thead>
                <tr className="">
                    <th className="id">Id</th>
                    <th className="name">Name</th>
                    <th className="image">Image</th>
                    <th className="price">Price</th>
                    <th className="action">action</th>
                </tr>
            </thead>
            {renderProduct()}
        </table>
    );
}

export default MyProduct;