import axios from "axios"
import { useEffect, useState } from "react"
import { json, useParams } from "react-router-dom"
import FormErrors from "../member/FormErrors";
import MyProduct from "./MyProduct"

function EditProduct(props) {
    let params = useParams();
    const [getinput, setinput] = useState({
        name: "",
        category: "",
        price: "",
        brand: "",
        status: "",
        company: "",
        detail: "",
        sale: "",
        image: ""
    })
    const [errors, seterrors] = useState({})
    const [id, setid] = useState([])
    const [getbrand, setbrand] = useState({})
    const [getcategory, setcategory] = useState({})
    useEffect(() => {
        const userDAta = JSON.parse(localStorage["login"])
        setid(userDAta.Auth.id)
        let accessToken = userDAta.success.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        axios.get("http://localhost:8080/laravel/laravel/public/api/user/product/" + params.id, config)
            .then(res => {
                let up = res.data.data
                setinput({
                    name: up.name,
                    category: up.id_category,
                    price: up.price,
                    brand: up.id_brand,
                    status: up.status,
                    company: up.company_profile,
                    detail: up.detail,
                    sale: up.sale,
                    image: up.image
                })
            })
            .catch(error => console.log(error))
        axios.get("http://localhost:8080/laravel/laravel/public/api/category-brand")
            .then(res => {
                setcategory(res.data.category)
                setbrand(res.data.brand)
            })
            .catch(error => console.log(error))
    }, [])
    function optioncategory() {
        if (getcategory.length > 0) {
            return getcategory.map((value, key) => {
                return (
                    <option key={key} value={value.id}>{value.category}</option>
                )
            })
        }

    }
    function optionbrand() {
        if (getbrand.length > 0) {
            return getbrand.map((value, key) => {
                return (
                    <option key={key} value={value.id}>{value.brand}</option>
                )
            })
        }
    }
    function inputsale() {
        if (getinput.status == "0") {
            return (
                <input type="text" placeholder="Sale" name="sale" onChange={Handleinput} />
            )
        }
    }
    const hinhanh = getinput.image
    const [getMessage, setMessage] = useState([])
    function checkanh() {
        return Object.keys(hinhanh).map((value, key) => {
            return (
                <li className="checkanh" key={key}>
                    <img src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" + id + "/" + hinhanh[value]} />
                    <input type="checkbox" value={hinhanh[value]} onClick={addMessage}></input>
                </li>
            )
        })
    }
    function addMessage(e) {
        let xx = e.target.value;
        let checked = e.target.checked
        if (checked) {
            setMessage(oldMessages => [xx, ...oldMessages])
        } else {
            if (getMessage.includes(xx) == true) {
                setMessage(getMessage.splice(xx, getMessage), 1)
            }
        }
    }
    function Handleinput(e) {
        const nameInput = e.target.name
        const valueInput = e.target.value
        setinput(state => ({ ...state, [nameInput]: valueInput }))
    }
    const [getfile, setfile] = useState("")
    function HandleFile(e) {
        setfile(e.target.files)
    }
    const Duoifile = ["png", "jpg", "jpeg", "PNG", "JPG"]
    function HandleForm(e) {
        e.preventDefault()
        let errorSubmit = {}
        let flag = true
        if (getinput.name == "") {
            errorSubmit.name = "vui long nhap name"
            flag = false
        }
        if (getinput.price == "") {
            errorSubmit.price = "vui long nhap price"
            flag = false
        }
        if (getinput.company == "") {
            errorSubmit.company = "vui long nhap company"
            flag = false
        }
        if (getinput.detail == "") {
            errorSubmit.detail = "vui long nhap detail"
            flag = false
        }
        if (getinput.status == "") {
            errorSubmit.status = "vui long nhap status"
            flag = false
        }
        if (getfile == "") {
            errorSubmit.files = "vui long them hinh anh "
            flag = false
        } else {
            const nameFile = getfile[0].name.split(".", 3);
            const nameFiles = nameFile[1]
            if (getfile[0].size > 1024 * 1024) {
                errorSubmit.files = "anh lon hon 1mb"
                flag = false;
            } else if (Duoifile.includes(nameFiles) == false) {
                errorSubmit.files = "anh khong dung dinh dang"
                flag = false;
            }
        }
        if (!flag) {
            seterrors(errorSubmit)
        } else {
            seterrors({})
            const userDAta = JSON.parse(localStorage["login"])
            let accessToken = userDAta.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            let url = "http://localhost:8080/laravel/laravel/public/api/user/edit-product/" + params.id
            const formData = new FormData()
            formData.append("name", getinput.name)
            formData.append("price", getinput.price)
            formData.append("category", getinput.category)
            formData.append("brand", getinput.brand)
            formData.append("company", getinput.company)
            formData.append("detail", getinput.detail)
            formData.append("status", getinput.status)
            formData.append("sale", getinput.sale)
            Object.keys(getfile).map((item, i) => {
                formData.append("file[]", getfile[item])
            })
            Object.keys(getMessage).map((item, i) => {
                formData.append("avatarCheckBox[]", getMessage[item])
            })
            axios.post(url, formData, config)
                .then(res => {
                    if (res.data.errors) {
                        seterrors(res.data.errors)
                    } else {
                        alert("edit product thanh cong")
                        console.log(res)
                    }
                })
        }
    }
    return (
        <div className="col-sm-4 col-sm-offset-1">
            <div className="login-form">
                <h2>Edit product</h2>
                <FormErrors errors={errors}> </FormErrors>
                <form action="#" encType="multipart/form-data" onSubmit={HandleForm}>
                    <input type="text" placeholder="Name" name="name" value={getinput.name} onChange={Handleinput} />
                    <input type="text" placeholder="Price" name="price" value={getinput.price} onChange={Handleinput} />
                    <select name="category" value={getinput.category} onChange={Handleinput}>
                        <option value="">category</option>
                        {optioncategory()}
                    </select><br /><br />
                    <select name="brand" value={getinput.brand} onChange={Handleinput}>
                        <option value="">brand</option>
                        {optionbrand()}
                    </select><br /><br />
                    <select name="status" value={getinput.status} onChange={Handleinput}>
                        <option value="">status</option>
                        <option value={1}>new</option>
                        <option value={0}>sale</option>
                    </select><br /><br />
                    {inputsale()}
                    <input type="text" placeholder="Company" name="company" value={getinput.company} onChange={Handleinput} />
                    <input type="file" placeholder="anh" name="files" onChange={HandleFile} multiple />
                    <ul>{checkanh()}</ul>
                    <textarea name="detail" rows={11} placeholder="Detail" defaultValue={""} value={getinput.detail} onChange={Handleinput} />
                    <button type="submit" className="btn btn-default">Signup</button>
                </form>
            </div>
        </div>
    );
}

export default EditProduct;