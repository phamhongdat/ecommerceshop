import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function ProductDetail() {
    let params = useParams()
    const [getData, setData] = useState([])
    const [getDa, setDa] = useState([])
    const [getImg, setImg] = useState([])
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/laravel/public/api/product/detail/" + params.id)
            .then(res => {
                setData(res.data.data)
                setDa(JSON.parse(res.data.data.image))
                let a = JSON.parse(res.data.data.image)
                setImg(a[0])
            })
            .catch(error => console.log(error))
    }, [])
    function handlechange(e) {
        const id = e.target.id
        setImg(id)
    }
    function anh() {
        if (Object.keys(getDa).length > 0) {
            return Object.keys(getDa).map((value, key) => {
                return (
                    <div className="anhactive" key={key}>
                        <a className="anhimg" onClick={handlechange} href><img id={getDa[value]} src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" + getData.id_user + "/" + getDa[value]} alt="" /></a>
                    </div>
                )
            })
        }
    }
    function renderProduct() {
        if (Object.keys(getData).length > 0) {
            return (
                <div className="product-details">{/*product-details*/}
                    <div className="col-sm-5">
                        <div className="view-product">
                            <img src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" + getData.id_user + "/" + getImg} alt="" />
                            <a href="images/product-details/1.jpg" rel="prettyPhoto"><h3>ZOOM</h3></a>
                        </div>
                        <div id="similar-product" className="carousel slide" data-ride="carousel">
                            < div className=""  >
                                {anh()}
                            </div >
                            <a className="left item-control" href="#similar-product" data-slide="prev">
                                <i className="fa fa-angle-left" />
                            </a>
                            <a className="right item-control" href="#similar-product" data-slide="next">
                                <i className="fa fa-angle-right" />
                            </a>
                        </div>
                    </div>
                    <div className="col-sm-7">
                        <div className="product-information">{/*/product-information*/}
                            <img src="images/product-details/new.jpg" className="newarrival" alt="" />
                            <h2>{getData.name}</h2>
                            <p>Web ID: 1089772</p>
                            <img src="images/product-details/rating.png" alt="" />
                            <span>
                                <span>US ${getData.price}</span>
                                <label>Quantity:</label>
                                <input type="text" defaultValue={3} />
                                <button type="button" className="btn btn-fefault cart">
                                    <i className="fa fa-shopping-cart" />
                                    Add to cart
                                </button>
                            </span>
                            <p><b>Availability:</b> In Stock</p>
                            <p><b>Condition:</b> New</p>
                            <p><b>Brand:</b> E-SHOPPER</p>
                            <a href><img src="images/product-details/share.png" className="share img-responsive" alt="" /></a>
                        </div>
                    </div>
                </div>
            )
        }
    }
    return (
        <div>
            {renderProduct()}
        </div>
    );
}

export default ProductDetail;