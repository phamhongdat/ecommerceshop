import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Blog from './Blog/List';
import BlogDetail from './Blog/Detail';
import Home from './Home';
import Register from './member/Register';
import Login from './member/Login';
import Update from './member/Update';
import AddProduct from './Product/AddProduct';
import MyProduct from './Product/MyProduct';
import EditProduct from './Product/EditProduct';
import ProductDetail from './Product/ProductDetail';
import Cart from './Product/Cart';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route index path='/' element={<Home />} />
          <Route path='/blog/list' element={<Blog />} />
          <Route path='/blog/detail/:id' element={<BlogDetail />} />
          <Route path='/member/register' element={<Register />} />
          <Route path='/member/login' element={<Login />} />
          <Route path='/member/update' element={<Update />} />
          <Route path='/product/addproduct' element={<AddProduct />} />
          <Route path='/product/myproduct' element={<MyProduct />} />
          <Route path='/product/editproduct/:id' element={<EditProduct />} />
          <Route path='/product/productdetail/:id' element={<ProductDetail />} />
          <Route path='/product/cart' element={<Cart />} />
        </Routes>
      </App>
    </Router>
  </React.StrictMode>
);
reportWebVitals();
