import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import FormErrors from "./FormErrors";

function Update() {
    let params = useParams()
    const [getinput, setinput] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: "",
        level: "",
        id: ""
    })
    useEffect(() => {
        const userDAta = JSON.parse(localStorage["login"])
        const userdata = JSON.parse(localStorage["Object"])
        setinput({
            name: userDAta.Auth.name,
            email: userDAta.Auth.email,
            password: "",
            phone: userDAta.Auth.phone,
            address: userDAta.Auth.address,
            id: userDAta.Auth.id
        })
    }, [])
    const [errors, seterrors] = useState({})
    function Handleinput(e) {
        const nameInput = e.target.name
        const valueInput = e.target.value
        setinput(state => ({ ...state, [nameInput]: valueInput }))
    }
    const [getavatar, setAvatar] = useState("")
    const [getfile, setfile] = useState("")
    function HandleFile(e) {
        const file = e.target.files
        let render = new FileReader();
        render.onload = (e) => {
            setAvatar(e.target.result)
            setfile(file[0])
        }
        render.readAsDataURL(file[0])
    }
    const Duoifile = ["png", "jpg", "jpeg", "PNG", "JPG"]
    function HandleForm(e) {
        e.preventDefault()
        let errorSubmit = {}
        let flag = true
        let testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        if (getinput.name == "") {
            errorSubmit.name = "vui long nhap name"
            flag = false
        }
        if (getinput.email == "") {
            errorSubmit.email = "vui long nhap email"
            flag = false
        } else if (!testEmail.test(getinput.email)) {
            errorSubmit.email = "vui long nhap email dung dinh dang"
            flag = false
        }
        if (getinput.phone == "") {
            errorSubmit.phone = "vui long nhap phone"
            flag = false
        }
        if (getinput.address == "") {
            errorSubmit.address = "vui long nhap address"
            flag = false
        }
        if (getfile) {
            let anh = getfile.type.split("image/")
            if (!Duoifile.includes(anh[1])) {
                errorSubmit.file = "file chon khong phai la dinh dang anh"
                flag = false
            }
            else if (getfile.size > 1024 * 1024) {
                errorSubmit.file = "anh avatar lon hon 1MB"
                flag = false
            }
        }
        if (!flag) {
            seterrors(errorSubmit)
        } else {
            seterrors({})
            const userDAta = JSON.parse(localStorage["login"])
            const userpass = JSON.parse(localStorage["Object"])
            const formData = new FormData()
            formData.append("name", getinput.name)
            formData.append("email", getinput.email)
            formData.append("password", getinput.password ? getinput.password : userpass.password)
            formData.append("phone", getinput.phone)
            formData.append("address", getinput.address)
            formData.append("avatar", getavatar ? getavatar : userDAta.Auth.avatar)
            let url = "http://localhost:8080/laravel/laravel/public/api/user/update/" + getinput.id
            let accessToken = userDAta.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            axios.post(url, formData, config)
                .then(res => {
                    if (res.data.errors) {
                        seterrors(res.data.errors)
                    } else {
                        alert("update user thanh cong")
                        console.log(res)
                    }
                })
        }
    }
    return (
        <div className="col-sm-4 col-sm-offset-1">
            <div className="login-form">{/*login form*/}
                <h2>Update User</h2>
                <FormErrors errors={errors}> </FormErrors>
                <form action="#" onSubmit={HandleForm} encType="multipart/form-data">
                    <input type="text" placeholder="Name" name="name" value={getinput.name} onChange={Handleinput} />
                    <input type="email" placeholder="Email Address" name="email" value={getinput.email} onChange={Handleinput} readOnly />
                    <input type="password" placeholder="Password" name="password" value={getinput.password} onChange={Handleinput} />
                    <input type="phone" placeholder="phone" name="phone" value={getinput.phone} onChange={Handleinput} />
                    <input type="text" name="address" placeholder="address" value={getinput.address} onChange={Handleinput} />
                    <input type="file" placeholder="Avatar" name="avatar" onChange={HandleFile} />
                    <button type="submit" className="btn btn-default">Signup</button>
                </form>
            </div>{/*/login form*/}
        </div>
    );
}

export default Update;