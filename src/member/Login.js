import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import FormErrors from "./FormErrors";

function Login(props) {
    const navigate = useNavigate()
    const [getinput, setinput] = useState({
        email: "",
        password: "",
        level: ""
    })
    const [errors, seterrors] = useState({})
    function Handleinput(e) {
        const nameInput = e.target.name
        const valueInput = e.target.value
        setinput(state => ({ ...state, [nameInput]: valueInput }))
    }
    function HandleForm(e) {
        e.preventDefault()
        let errorSubmit = ({})
        let flag = true
        let testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        if (getinput.email == "") {
            errorSubmit.email = "vui long nhap email"
            flag = false
        }
        else if (!testEmail.test(getinput.email)) {
            errorSubmit.email = "vui long nhap email dung dinh dang"
            flag = false
        }
        if (getinput.password == "") {
            errorSubmit.password = "vui long nhap password"
            flag = false
        }
        if (getinput.level == "") {
            errorSubmit.level = "vui long nhap level"
            flag = false
        }
        if (!flag) {
            seterrors(errorSubmit)
        }
        else {
            let dataa = {
                email: getinput.email,
                password: getinput.password,
                level: getinput.level,
            }
            seterrors({})
            axios.post("http://localhost:8080/laravel/laravel/public/api/login", dataa)
                .then(res => {
                    if (res.data.errors) {
                        seterrors(res.data.errors)
                    } else { 
                        navigate("/")
                        localStorage.setItem("Object", JSON.stringify(dataa))
                        localStorage.setItem("login", JSON.stringify(res.data))
                    }
                })
        }
    }
    return (
        <div className="col-sm-4 col-sm-offset-1">
            <div className="login-form">{/*login form*/}
                <h2>Login to your account</h2>
                <FormErrors errors={errors}> </FormErrors>
                <form action="#" onSubmit={HandleForm} encType="multipart/form-data">
                    <input type="email" placeholder="Email Address" name="email" onChange={Handleinput} />
                    <input type="password" placeholder="Password" name="password" onChange={Handleinput} />
                    <input type="number" placeholder="Level" name="level" onChange={Handleinput} />
                    <span>
                        <input type="checkbox" className="checkbox" />
                        Keep me signed in
                    </span>
                    <button type="submit" className="btn btn-default">Login</button>
                </form>
            </div>{/*/login form*/}
        </div>
    );
}

export default Login;