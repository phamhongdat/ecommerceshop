import { render } from "@testing-library/react";
import axios from "axios";
import { useState } from "react";
import FormErrors from "./FormErrors";

function Register(props) {
    const [getinput, setinput] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: ""
    })
    const [errors, seterrors] = useState({})
    function Handleinput(e) {
        const nameInput = e.target.name
        const valueInput = e.target.value
        setinput(state => ({ ...state, [nameInput]: valueInput }))
    }
    const [getavatar, setAvatar] = useState("")
    const [getfile, setfile] = useState("")
    function HandleFile(e) {
        const file = e.target.files
        let render = new FileReader();
        render.onload = (e) => {
            setAvatar(e.target.result)
            setfile(file[0])
        }
        render.readAsDataURL(file[0])
    }
    const Duoifile = ["png", "jpg", "jpeg", "PNG", "JPG"]
    function HandleForm(e) {
        e.preventDefault()
        let errorSubmit = {}
        let flag = true
        let testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        if (getinput.name == "") {
            errorSubmit.name = "vui long nhap name"
            flag = false
        }
        if (getinput.email == "") {
            errorSubmit.email = "vui long nhap email"
            flag = false
        } else if (!testEmail.test(getinput.email)) {
            errorSubmit.email = "vui long nhap email dung dinh dang"
            flag = false
        }
        if (getinput.password == "") {
            errorSubmit.password = "vui long nhap password"
            flag = false
        }
        if (getinput.phone == "") {
            errorSubmit.phone = "vui long nhap phone"
            flag = false
        }
        if (getinput.address == "") {
            errorSubmit.address = "vui long nhap address"
            flag = false
        }
        if (getfile == "") {
            errorSubmit.file = "vui long them avatar"
            flag = false
        }
        else {
            let anh = getfile.type.split("image/")
            if (!Duoifile.includes(anh[1])) {
                errorSubmit.file = "file chon khong phai la dinh dang anh"
                flag = false
            }
            else if (getfile.size > 1024 * 1024) {
                errorSubmit.file = "anh avatar lon hon 1MB"
                flag = false
            }
        }
        if (!flag) {
            seterrors(errorSubmit)
        } else {
            let data = {
                name: getinput.name,
                email: getinput.email,
                password: getinput.password,
                phone: getinput.phone,
                address: getinput.address,
                avatar: getavatar,
                level: 0
            }
            seterrors({})
            axios.post("http://localhost:8080/laravel/laravel/public/api/register", data)
                .then(res => {
                    if (res.data.errors) {
                        seterrors(res.data.errors)
                    } else {
                        alert("dang ki thanh cong")
                        console.log(res)
                    }
                })
        }
    }
    return (
        <div className="col-sm-4 col-sm-offset-1">
            <div className="login-form">{/*login form*/}
                <h2>Register to your account</h2>
                <FormErrors errors={errors}> </FormErrors>
                <form action="#" onSubmit={HandleForm} encType="multipart/form-data">
                    <input type="text" placeholder="Name" name="name" onChange={Handleinput} />
                    <input type="email" placeholder="Email Address" name="email" onChange={Handleinput} />
                    <input type="password" placeholder="Password" name="password" onChange={Handleinput} />
                    <input type="phone" placeholder="phone" name="phone" onChange={Handleinput} />
                    <input type="file" placeholder="Avatar" name="avatar" onChange={HandleFile} />
                    <input type="number" placeholder="Level" name="level" value="0" onChange={Handleinput} />
                    <input type="text" name="address" placeholder="address" onChange={Handleinput} />
                    <span>
                        <input type="checkbox" className="checkbox" />
                        Keep me signed in
                    </span>
                    <button type="submit" className="btn btn-default">Login</button>
                </form>
            </div>{/*/login form*/}
        </div>
    );
}

export default Register;