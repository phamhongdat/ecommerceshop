import './App.css';
import Header from './Blog/Header';
import MenuLeft from './Blog/MenuLeft';
import Foodter from './Blog/Foodter';
import { useLocation } from 'react-router-dom';
import MenuAccount from './member/MenuAccount';
import { AppProvider, Context } from './Product/Context';
import { useState } from 'react';

function App(props) {
  let params1 = useLocation()
  const [getQyt, setQty] = useState()
  function qtycart(data) {
    setQty(data)
  }
  return (
    <>
      <Context.Provider value={{
        getQyt: getQyt,
        qtycart: qtycart
      }}>
        <Header />
        <section>
          <div className="container">
            <div className="row">
              {params1['pathname'].includes("update") ? <MenuAccount /> : <MenuLeft />}
              {props.children}
            </div>
          </div>
        </section>
        <Foodter />
      </Context.Provider>
    </>
  )
}

export default App;
